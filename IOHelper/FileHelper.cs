﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOHelper
{
    public class FileHelper
    {
        public static List<string> LoadFileNameFromFolder(string path, string searchPattern)
        {
            string[] filePaths = Directory.GetFiles(path, searchPattern, SearchOption.AllDirectories);
            var result = filePaths.OrderBy(x => x).ToList();
            return result;
        }

        /// <summary>
        /// Rename a file with a date base. Add prefix YYYY-MM-DD-HH-MM-SS such as 2016-07-29-15-3046 to an existing file name
        /// </summary>
        public static void RenameFileDateBase(string fullPath,string outputFolder, DateTime dateBase, bool moveToNewFolder = true,string format = "yyyy-MM-dd HH-mm-ss")
        {
            if (string.IsNullOrEmpty(outputFolder))
            {
                outputFolder = Path.GetDirectoryName(fullPath);
            }
            //get fileName
            var fileName = Path.GetFileName(fullPath);
            
            //form a new file name with YYYY-MM-DD-HH-MM-SS
            var newNamePrefix = dateBase.ToString(format);
            if (!fileName.StartsWith(newNamePrefix))
            {
                var newFileName = string.Format("{0} - {1}", newNamePrefix, fileName);
                var newFullFileName = outputFolder + "\\" +newFileName;
                if(moveToNewFolder)
                {
                    System.IO.File.Copy(fullPath, newFullFileName);
                }
                else
                {
                    //rename only
                    System.IO.File.Move(fullPath, newFullFileName);
                }
               
            }
        }
        /// <summary>
        /// Rename a file with a date base. Add prefix YYYY-MM-DD-HH-MM-SS such as 2016-07-29-15-3046 to an existing file name
        /// </summary>
        public static void RecoverFileDateBase(string fullPath, string format = "yyyy-MM-dd HH-mm-ss")
        {
            //get fileName
            var fileName = Path.GetFileName(fullPath);

            //form a new file name with YYYY-MM-DD-HH-MM-SS
            if (fileName.Length > 22)
            {
                try
                {
                    //Check date before rename
                    DateTime.Parse(fileName.Substring(0,10));//2018-02-15
                    int oldLength = fileName.Length - 22;//2018-02-15 22-46-04 - 2018-02-15-6556.mp4
                    var newName = fileName.Substring(22, oldLength);
                    var newFullFileName = fullPath.Replace(fileName, newName);
                    System.IO.File.Move(fullPath, newFullFileName);
                }
                catch
                {

                }
               
            }
            
        }

        /// <summary>
        /// Rename a file with a date base. Add prefix YYYY-MM-DD-HH-MM-SS such as 2016-07-29-15-3046 to an existing file name. App can be Viber or skype or somethings else
        /// </summary>
        public static void RenameFileDateBaseApp(string fullPath, DateTime dateBase, string app, string format = "yyyy-MM-dd HH-mm-ss")
        {
            //get fileName
            var fileName = Path.GetFileName(fullPath);

            //get extension 
            string ext = Path.GetExtension(fileName);

            //form a new file name with YYYY-MM-DD-HH-MM-SS
            var newNamePrefix = dateBase.ToString(format);
            var currentDate = DateTime.Now.Ticks;
            if (!fileName.StartsWith(newNamePrefix))
            {
                var newFileName = string.Format("{0} - {1}-{2}{3}", newNamePrefix, app, currentDate, ext );//appent currentDate to avoid duplicating
                var newFullFileName = fullPath.Replace(fileName, newFileName);
                System.IO.File.Move(fullPath, newFullFileName);
            }
        }

        public static void CreateChildFoldersYYYYMMDD(string path, string searchPattern)
        {
            string[] filePaths = Directory.GetFiles(path, searchPattern, SearchOption.AllDirectories);
            var files = filePaths.OrderBy(x => x).ToList();
            var dic = new Dictionary<string, List<string>>();
            for (int i = 0; i < files.Count; i++)
            {
                try
                {
                    //get onlye file name 

                    var dtName = System.IO.Path.GetFileName(files[i]).Substring(0, 10);//Get first 2019-10-06
                    var date = DateTime.Parse(dtName);//such as 2019-10-06
                    if (!dic.ContainsKey(dtName))
                    {
                        dic[dtName] = new List<string>();
                        dic[dtName].Add(files[i]);
                    }
                    else
                    {
                        dic[dtName].Add(files[i]);
                    }
                }
                catch
                {
                }

            }

            //create folder inside
            foreach (var entry in dic)
            {
                //create folder first
                var newFolder = $"{path}\\{entry.Key.Replace("-", "")}";
                System.IO.Directory.CreateDirectory(newFolder);
                foreach (var file in entry.Value)
                {
                    var newPath = $"{newFolder}\\{System.IO.Path.GetFileName(file)}";
                    System.IO.File.Move(file, newPath);
                }
            }

        }

        public static long GetFileSizeSumFromDirectory(string searchDirectory)
        {
            var files = Directory.EnumerateFiles(searchDirectory);

            var currentSize = (from file in files let fileInfo = new FileInfo(file) select fileInfo.Length).Sum();

            var directories = Directory.EnumerateDirectories(searchDirectory);

            var subDirSize = (from directory in directories select GetFileSizeSumFromDirectory(directory)).Sum();

            return currentSize + subDirSize;
        }
    }
}
