﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
namespace PhotoMover.Core.Test
{
    [TestClass]
    public class MetaDataVideoReaderTest
    {
        [TestMethod]
        public void GetDateModified_OK()
        {
            var dir = $"{Directory.GetCurrentDirectory()}\\SampleData\\VID20181209141240.mp4";
            var dt = MetaDataVideoReader.GetDateModified(dir);
        }
        [TestMethod]
        public void GetDateOriginMediaCreated_OK()
        {
            var dir = $"{Directory.GetCurrentDirectory()}\\SampleData\\VID20181209141240.mp4";
            var dt = MetaDataVideoReader.GetDateOriginMediaCreated(dir);
        }

        [TestMethod]
        public void GetMediaCreatedOfMOV_OK()
        {
            var dir = $"{Directory.GetCurrentDirectory()}\\SampleData\\MediaCreated.MOV";
            var dt = MetaDataVideoReader.GetMediaCreatedOfMOV(dir);
        }
    }
}
