﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace PhotoMover.Core.Test
{
    [TestClass]
    public class MetaDataPhotoReaderTest
    {
        [TestMethod]
        public void GetDateTaken_OK()
        {
            var dir = $"{Directory.GetCurrentDirectory()}\\SampleData\\IMG_3277.jpg";
            var dt = MetaDataPhotoReader.GetDateTaken(dir);
        }

        [TestMethod]
        public void GetGetDateModified_OK()
        {
            var dir = $"{Directory.GetCurrentDirectory()}\\SampleData\\IMG_3277.jpg";
            var dt = MetaDataPhotoReader.GetDateModified(dir);
        }
        [TestMethod]
        public void GetGetDateModified_Then_Move_OK()
        {
            var dir = $"{Directory.GetCurrentDirectory()}\\SampleData\\IMG_3277.jpg";
            var dt = MetaDataPhotoReader.GetDateModified(dir);
            var newFolder = $"{Directory.GetCurrentDirectory()}\\SampleData\\Test";
            Directory.CreateDirectory(newFolder);
            File.Move(dir, newFolder + $"\\IMG_3277_{Guid.NewGuid().ToString()}.jpg");
        }
        [TestMethod]
        public void GetGetDateModified_Then_Rename_OK()
        {
            
            var dir = $"{Directory.GetCurrentDirectory()}\\SampleData\\IMG_3277.jpg";
            var fileName = Path.GetFileName(dir);
            var dt = MetaDataPhotoReader.GetDateModified(dir);
            var newFolder = $"{Directory.GetCurrentDirectory()}\\SampleData";
            Directory.CreateDirectory(newFolder);
            File.Move(dir, newFolder + $"\\IMG_3277_{Guid.NewGuid().ToString()}.jpg");
        }
    }
}
