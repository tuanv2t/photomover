﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
namespace PhotoMover.Core
{
    public class MetaDataPhotoReader
    {
        public static DateTime GetDateTaken(string filePath)
        {
            //Image myImage = Image.FromFile(filePath);
            //PropertyItem propItem = myImage.GetPropertyItem(36867);
            //string dateTaken = new Regex(":").Replace(Encoding.UTF8.GetString(propItem.Value), "-", 2);
            //myImage.Dispose();//NOT GOOD, MUST USE using to force dispose with exception
            using (var myImage = Image.FromFile(filePath))
            {
                PropertyItem propItem = myImage.GetPropertyItem(36867);
                string dateTaken = new Regex(":").Replace(Encoding.UTF8.GetString(propItem.Value), "-", 2);
                return DateTime.Parse(dateTaken);
            }
           

        }
        public static DateTime GetDateModified(string filePath)
        {
            var dt = File.GetLastWriteTime(filePath);
            return dt;
        }

    }
}
