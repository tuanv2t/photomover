﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Microsoft.WindowsAPICodePack.Shell;
using System.Diagnostics;

namespace PhotoMover.Core
{
    public class MetaDataVideoReader
    {
        public static DateTime GetDateModified(string filePath)
        {
            var dt = File.GetLastWriteTime(filePath);
            return dt;
        }
       
        public static DateTime GetDateOriginMediaCreated(string filePath)
        {
            //Shell32.Shell shell = new Shell32.Shell();
            //Shell32.Folder objFolder = shell.NameSpace(System.IO.Path.GetDirectoryName(filePath));
            //Shell32.FolderItem folderItem = objFolder.ParseName(System.IO.Path.GetFileName(filePath));
            ////objFolder.GetDetailsOf(null, 177);
            //List<string> arrHeaders = new List<string>();
            //List<int> properties = new List<int>();
            //for (int i = 0; i < short.MaxValue; i++)
            //{
            //    string header = objFolder.GetDetailsOf(null, i);
            //    if (!String.IsNullOrEmpty(header))
            //    {
            //        //objFolder.GetDetailsOf(folderItem, i);
            //        arrHeaders.Add(header);
            //        properties.Add(i);
            //    }
                
            //}
            //StringBuilder sb = new StringBuilder();
            //for (int i = 0; i < arrHeaders.Count; i++)
            //{
            //    try
            //    {
            //        sb.Append(string.Format("{0}\t{1}: {2}\n", i, arrHeaders[i], objFolder.GetDetailsOf(folderItem, properties[i])));
            //    }
            //    catch(Exception ex)
            //    {
            //    }
            //}
            //File.WriteAllText("C:\\PhotoMover test\\abc.txt",sb.ToString());
            var dt = File.GetLastWriteTime(filePath);
            return dt;
        }
        public static DateTime? GetMediaCreatedOfMOV(string filePath)
        {
            //http://www.mendipdatasystems.co.uk/extended-file-properties/4594398115
            //https://stackoverflow.com/questions/8351713/how-can-i-extract-the-date-from-the-media-created-column-of-a-video-file
            //File .MOV which can have Media Created is correct while Date Modified is wrong 
            // Shell32 does not run well on window 10 ???
            //Shell32.Shell shell = new Shell32.Shell();
            //Shell32.Folder objFolder = shell.NameSpace(System.IO.Path.GetDirectoryName(filePath));
            //Shell32.FolderItem folderItem = objFolder.ParseName(System.IO.Path.GetFileName(filePath));
            //string name = objFolder.GetDetailsOf(null, 191);

            //string value = objFolder.GetDetailsOf(folderItem, 191).Trim();
            //for(var i = 1;i < 311; i++)
            //{
            //    var s = objFolder.GetDetailsOf(folderItem, i);
            //    try
            //    {
            //        var ddd = DateTime.Parse(s);
            //        var HH = ddd.Minute;
            //    }
            //    catch(Exception ex)
            //    {

            //    }

            //}

            //Use this nuget : https://www.nuget.org/packages/Microsoft.WindowsAPICodePack-Shell
            using (var shell1 = ShellObject.FromParsingName(filePath))
            {
                IShellProperty prop = shell1.Properties.System.ItemDate;
                return  (DateTime)prop.ValueAsObject;
            }

        }
        /// <summary>
        /// Sometime the media created date is the right created date
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static DateTime GetDateModifiedMOV(string filePath)
        {
            var dateModified = File.GetLastWriteTime(filePath);
            var mediaCreatedDate = GetMediaCreatedOfMOV(filePath);
            if(mediaCreatedDate!=null && mediaCreatedDate.Value < dateModified)
            {
                return mediaCreatedDate.Value;
            }
            return dateModified;
        }

    }
}
