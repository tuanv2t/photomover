﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using IOHelper;

namespace PhotoMover
{
    public partial class FolderSize : Form
    {
        public FolderSize()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Get child folders
            string[] subdirectoryEntries = Directory.GetDirectories(txtFolder.Text);


            var minimumSize = long.Parse(textBox1.Text);
            var result = new List<FolderInfo>();
            foreach(var dir in subdirectoryEntries)
            {
                var size = FileHelper.GetFileSizeSumFromDirectory(dir);
                if(size < minimumSize)
                {
                    result.Add(new FolderInfo()
                    {
                        Path = dir,
                        Size = size,
                        Name = Path.GetFileName(dir)
                    });
                }
            }
            listView1.Items.Clear();

            //Show on list view 
            foreach(var dir in result)
            {
                
                var item = new ListViewItem(dir.Path);
                item.SubItems.Add(dir.Size.ToString());
                item.Tag = dir;
                if (checkBox1.Checked)
                {
                    if (dir.Name.Length == 8)//YYYYMMDD
                    {

                        listView1.Items.Add(item);
                    }
                }
                else
                {
                }
                
            }
            MessageBox.Show("Done");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!Directory.Exists(textBox2.Text))
            {
                Directory.CreateDirectory(textBox2.Text);
            }
            foreach(var item in listView1.Items)
            {
                //Move all to textBox2.Text
                try
                {
                    var dir = (FolderInfo)((ListViewItem)item).Tag;
                    var newFolder = $"{textBox2.Text}\\{dir.Name}";
                    Directory.Move(dir.Path, newFolder);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
              
            }
            MessageBox.Show("Done");
        }

        private void FolderSize_Load(object sender, EventArgs e)
        {

        }
    }
    class FolderInfo
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
    }
}
