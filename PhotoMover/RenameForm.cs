﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IOHelper;
using PhotoMover.Core;

namespace PhotoMover
{
    public partial class RenameForm : Form
    {
        public RenameForm()
        {
            InitializeComponent();
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            //check if the ouput exist or not 
            if(checkBoxMoveToNewFolder.Checked)
            {
                if (string.IsNullOrEmpty(txtOutPutFolder.Text.Trim()))
                {
                    MessageBox.Show("Please select output folder.");
                    return;
                }
                if (!Directory.Exists(txtOutPutFolder.Text.Trim()))
                {
                    //try to create
                    try
                    {
                        Directory.CreateDirectory(txtOutPutFolder.Text.Trim());
                    }
                    catch
                    {
                        MessageBox.Show("Unable to create ." + txtOutPutFolder.Text.Trim());
                        return;
                    }
                }
                if (txtOutPutFolder.Text.Trim().ToUpper().Equals(txtFolder.Text.Trim()))
                {

                    MessageBox.Show("Input and output folder must be different.");
                    return;
                }

            }
            else
            {
                txtOutPutFolder.Text = txtFolder.Text;
            }
          
            var files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.jpg");
            for (int i = 0; i < files.Count; i++)
            {
                try
                {
                    var dateTaken = MetaDataPhotoReader.GetDateTaken(files[i]);
                    FileHelper.RenameFileDateBase(files[i], txtOutPutFolder.Text.Trim(),dateTaken,checkBoxMoveToNewFolder.Checked);
                }
                catch
                {
                    //sometime a picture does not have date take ( example picture taken by yoosee camera ) => try to get its date modified 
                    var dateTaken = MetaDataPhotoReader.GetDateModified(files[i]);
                    FileHelper.RenameFileDateBase(files[i], txtOutPutFolder.Text.Trim(), dateTaken, checkBoxMoveToNewFolder.Checked);
                }
              
            }
            //MessageBox.Show("Done renaming jpg images.");

            //MP4
            files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.mp4");
            string fileName = "";
            for (int i = 0; i < files.Count; i++)
            {
                var dateModified = MetaDataVideoReader.GetDateModified(files[i]);
                //In my case, Nokia N808 use DateModified is the UTC time, so that it must be updated to local time, in my case is GMT+7
                //Nokia 1020 use right local time for DateModified ( file name starts with  WP_.. )
                //if (!Path.GetFileName(files[i]).StartsWith("WP_"))
                fileName = Path.GetFileName(files[i]);
                if(fileName[4]=='-' && fileName[7]== '-' && fileName[10]== '-')//2018-02-15-6556.mp4 //Format of NOKIA N808
                {
                     dateModified = dateModified.AddHours(7);//for N808
                    //dateModified = dateModified.AddHours(-3);//for N808, modify source for videos in Ha Lan
                    //dateModified = dateModified.AddHours(+2);
                }
                if (Path.GetFileName(files[i]).StartsWith("WP_")) //N1020
                {
                    //modify for videos in Ha Lan
                   // dateModified = dateModified.AddHours(-5);
                }

                FileHelper.RenameFileDateBase(files[i],txtOutPutFolder.Text.Trim(), dateModified, checkBoxMoveToNewFolder.Checked);
            }
            //MessageBox.Show("Done renaming mp4 videos.");

            files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.MOV");//iPHONE
            fileName = "";
            for (int i = 0; i < files.Count; i++)
            {
                var dateModified = MetaDataVideoReader.GetDateModifiedMOV(files[i]);
                //In my case, Nokia N808 use DateModified is the UTC time, so that it must be updated to local time, in my case is GMT+7
                //Nokia 1020 use right local time for DateModified ( file name starts with  WP_.. )
                //if (!Path.GetFileName(files[i]).StartsWith("WP_"))
                fileName = Path.GetFileName(files[i]);
                if (fileName[4] == '-' && fileName[7] == '-' && fileName[10] == '-')//2018-02-15-6556.mp4 //Format of NOKIA N808
                {
                    dateModified = dateModified.AddHours(7);//for N808
                    //dateModified = dateModified.AddHours(-3);//for N808, modify source for videos in Ha Lan
                    //dateModified = dateModified.AddHours(+2);
                }
                if (Path.GetFileName(files[i]).StartsWith("WP_")) //N1020
                {
                    //modify for videos in Ha Lan
                    // dateModified = dateModified.AddHours(-5);
                }

                FileHelper.RenameFileDateBase(files[i], txtOutPutFolder.Text.Trim(), dateModified, checkBoxMoveToNewFolder.Checked);
            }
            //MessageBox.Show("Done renaming MOV videos.");

            //Fix for wrong renaming later
            //var format = "yyyy-MM-dd HH-mm-ss";
            //var files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.mp4");
            //for (int i = 0; i < files.Count; i++)
            //{
            //    var dateModified = MetaDataVideoReader.GetDateModified(files[i]);

            //    var fileName = Path.GetFileName(files[i]);
            //    if (fileName.Contains("WP_"))
            //    {
            //        var wrongDateModified = dateModified.AddHours(7);
            //        //form a new file name with YYYY-MM-DD-HH-MM-SS

            //        var newNamePrefix = dateModified.ToString(format);
            //        var wrongNamePrefix = wrongDateModified.ToString(format);
            //        if (!fileName.StartsWith(newNamePrefix))
            //        {
            //            var newFullFileName = files[i].Replace(wrongNamePrefix, newNamePrefix);
            //            System.IO.File.Move(files[i], newFullFileName);
            //        }
            //    }

            //}

            //Create folder by YYYYMMDD 
            if (checkBoxCreateFoldersYYYYMMDD.Checked)
            {
                FileHelper.CreateChildFoldersYYYYMMDD(txtOutPutFolder.Text.Trim(), "*.*");
            }
            else
            {
            }
            MessageBox.Show("Done");
        }

        private void btnBrowser_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //recover the old name 
            var files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.jpg");
            for (int i = 0; i < files.Count; i++)
            {
                FileHelper.RecoverFileDateBase(files[i]);
            }
            //MessageBox.Show("Done recovering jpg images.");

            //MP4
            files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.mp4");
            for (int i = 0; i < files.Count; i++)
            {
                FileHelper.RecoverFileDateBase(files[i]);
            }
            //MessageBox.Show("Done recovering mp4 videos.");
            //MOV
            files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.MOV");
            for (int i = 0; i < files.Count; i++)
            {
                FileHelper.RecoverFileDateBase(files[i]);
            }
            MessageBox.Show("DONE");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog2.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtOutPutFolder.Text = folderBrowserDialog2.SelectedPath;
            }
        }

        private void RenameForm_Load(object sender, EventArgs e)
        {

        }
    }
}
