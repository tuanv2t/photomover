﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using IOHelper;

namespace PhotoMover
{
    public partial class MoveFiles : Form
    {
        public MoveFiles()
        {
            InitializeComponent();
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtOutPutFolder.Text.Trim()))
            {
                MessageBox.Show("Please select output folder.");
                return;
            }
            if (!Directory.Exists(txtOutPutFolder.Text.Trim()))
            {
                //try to create
                try
                {
                    Directory.CreateDirectory(txtOutPutFolder.Text.Trim());
                }
                catch
                {
                    MessageBox.Show("Unable to create ." + txtOutPutFolder.Text.Trim());
                    return;
                }
            }
            if (txtOutPutFolder.Text.Trim().ToUpper().Equals(txtFolder.Text.Trim()))
            {

                MessageBox.Show("Input and output folder must be different.");
                return;
            }

            var files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.*");
            for (int i = 0; i < files.Count; i++)
            {
                

            }

        }
    }
}
