﻿using IOHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoMover
{
    public partial class RecoverLittleFolders : Form
    {
        public RecoverLittleFolders()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Get child folders
            string[] subdirectoryEntries = Directory.GetDirectories(txtFolder.Text);


            var minimumSize = long.Parse(textBox1.Text);
            var result = new List<FolderInfo>();
            foreach (var dir in subdirectoryEntries)
            {
                var size = FileHelper.GetFileSizeSumFromDirectory(dir);
                if (size < minimumSize)
                {
                    result.Add(new FolderInfo()
                    {
                        Path = dir,
                        Size = size,
                        Name = Path.GetFileName(dir)
                    });
                }
            }
            listView1.Items.Clear();

            //Show on list view 
            foreach (var dir in result)
            {

                var item = new ListViewItem(dir.Path);
                item.SubItems.Add(dir.Size.ToString());
                item.Tag = dir;
                if (checkBox1.Checked)
                {
                    if (dir.Name.Length == 8)//YYYYMMDD
                    {

                        listView1.Items.Add(item);
                    }
                }
                else
                {
                }

            }
            MessageBox.Show("Done");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            foreach (var item in listView1.Items)
            {
                try
                {
                    var dir = (FolderInfo)((ListViewItem)item).Tag;
                    //get all files 
                    var files = FileHelper.LoadFileNameFromFolder(dir.Path, "*.*");
                    for (int i = 0; i < files.Count; i++)
                    {
                        try
                        {
                            var newPath = $"{txtFolder.Text}\\{Path.GetFileName(files[i])}";
                            File.Move(files[i], newPath);
                        }
                        catch (Exception ex1)
                        {

                            MessageBox.Show(ex1.Message);
                        }

                    }

                    //Check empty folder => Delete 
                    if (IsEmptyFolder(dir.Path))
                    {
                        Directory.Delete(dir.Path);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            listView1.Items.Clear();
            MessageBox.Show("Done");
        }

        private bool IsEmptyFolder(string folder)
        {
            var files = FileHelper.LoadFileNameFromFolder(folder, "*.*");
            var sub = Directory.GetDirectories(folder);
            return files.Count == 0 && sub.Count() == 0;
        }
    }
}
