﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using IOHelper;
using PhotoMover.Core;

namespace PhotoMover
{
    public partial class RenameViber : Form
    {
        public RenameViber()
        {
            InitializeComponent();
        }

        private void btnBrowser_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Do you want to rename Viber files ?") != DialogResult.OK)
            {
                return;
            }
            var files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.jpg");
            for (int i = 0; i < files.Count; i++)
            {
                var modifiedDate = MetaDataPhotoReader.GetDateModified(files[i]);
                FileHelper.RenameFileDateBaseApp(files[i], modifiedDate, "Viber");
            }
           // MessageBox.Show("Done renaming jpg images.");

            //MP4
            files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.mp4");
            for (int i = 0; i < files.Count; i++)
            {
                var dateModified = MetaDataVideoReader.GetDateModified(files[i]);
                //In my case, Nokia N808 use DateModified is the UTC time, so that it must be updated to local time, in my case is GMT+7
                //Nokia 1020 use right local time for DateModified ( file name starts with  WP_.. )
               // if (!Path.GetFileName(files[i]).StartsWith("WP_"))
               // {
                //    dateModified = dateModified.AddHours(7);//for N808
                    //dateModified = dateModified.AddHours(-3);//for N808, modify source for videos in Ha Lan
                    //dateModified = dateModified.AddHours(+2);
                //}
                //if (Path.GetFileName(files[i]).StartsWith("WP_")) //N1020
                //{
                    //modify for videos in Ha Lan
                    // dateModified = dateModified.AddHours(-5);
                //}

                FileHelper.RenameFileDateBaseApp(files[i], dateModified, "Viber");
            }
            MessageBox.Show("Done renaming mp4 videos.");

            //Fix for wrong renaming later
            //var format = "yyyy-MM-dd HH-mm-ss";
            //var files = FileHelper.LoadFileNameFromFolder(txtFolder.Text.Trim(), "*.mp4");
            //for (int i = 0; i < files.Count; i++)
            //{
            //    var dateModified = MetaDataVideoReader.GetDateModified(files[i]);

            //    var fileName = Path.GetFileName(files[i]);
            //    if (fileName.Contains("WP_"))
            //    {
            //        var wrongDateModified = dateModified.AddHours(7);
            //        //form a new file name with YYYY-MM-DD-HH-MM-SS

            //        var newNamePrefix = dateModified.ToString(format);
            //        var wrongNamePrefix = wrongDateModified.ToString(format);
            //        if (!fileName.StartsWith(newNamePrefix))
            //        {
            //            var newFullFileName = files[i].Replace(wrongNamePrefix, newNamePrefix);
            //            System.IO.File.Move(files[i], newFullFileName);
            //        }
            //    }

            //}
        }
    }
}
