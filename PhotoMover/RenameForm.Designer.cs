﻿namespace PhotoMover
{
    partial class RenameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.btnBrowser = new System.Windows.Forms.Button();
            this.btnRename = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.txtOutPutFolder = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.folderBrowserDialog2 = new System.Windows.Forms.FolderBrowserDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxMoveToNewFolder = new System.Windows.Forms.CheckBox();
            this.checkBoxCreateFoldersYYYYMMDD = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(116, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rename images by date taken";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, 71);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(289, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Rename video (*.mp4) by Date Modified";
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(120, 114);
            this.txtFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(626, 26);
            this.txtFolder.TabIndex = 2;
            // 
            // btnBrowser
            // 
            this.btnBrowser.Location = new System.Drawing.Point(756, 112);
            this.btnBrowser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnBrowser.Name = "btnBrowser";
            this.btnBrowser.Size = new System.Drawing.Size(86, 35);
            this.btnBrowser.TabIndex = 3;
            this.btnBrowser.Text = "...";
            this.btnBrowser.UseVisualStyleBackColor = true;
            this.btnBrowser.Click += new System.EventHandler(this.btnBrowser_Click);
            // 
            // btnRename
            // 
            this.btnRename.Location = new System.Drawing.Point(119, 236);
            this.btnRename.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(112, 35);
            this.btnRename.TabIndex = 4;
            this.btnRename.Text = "Rename";
            this.btnRename.UseVisualStyleBackColor = true;
            this.btnRename.Click += new System.EventHandler(this.btnRename_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(315, 236);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(236, 35);
            this.button1.TabIndex = 5;
            this.button1.Text = "Recover Name";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtOutPutFolder
            // 
            this.txtOutPutFolder.Location = new System.Drawing.Point(119, 170);
            this.txtOutPutFolder.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtOutPutFolder.Name = "txtOutPutFolder";
            this.txtOutPutFolder.Size = new System.Drawing.Size(626, 26);
            this.txtOutPutFolder.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(756, 170);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 35);
            this.button2.TabIndex = 7;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 118);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Input Folder:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 174);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Output Folder:";
            // 
            // checkBoxMoveToNewFolder
            // 
            this.checkBoxMoveToNewFolder.AutoSize = true;
            this.checkBoxMoveToNewFolder.Checked = true;
            this.checkBoxMoveToNewFolder.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMoveToNewFolder.Location = new System.Drawing.Point(120, 11);
            this.checkBoxMoveToNewFolder.Name = "checkBoxMoveToNewFolder";
            this.checkBoxMoveToNewFolder.Size = new System.Drawing.Size(170, 24);
            this.checkBoxMoveToNewFolder.TabIndex = 10;
            this.checkBoxMoveToNewFolder.Text = "Move to New folder";
            this.checkBoxMoveToNewFolder.UseVisualStyleBackColor = true;
            // 
            // checkBoxCreateFoldersYYYYMMDD
            // 
            this.checkBoxCreateFoldersYYYYMMDD.AutoSize = true;
            this.checkBoxCreateFoldersYYYYMMDD.Checked = true;
            this.checkBoxCreateFoldersYYYYMMDD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCreateFoldersYYYYMMDD.Location = new System.Drawing.Point(381, 11);
            this.checkBoxCreateFoldersYYYYMMDD.Name = "checkBoxCreateFoldersYYYYMMDD";
            this.checkBoxCreateFoldersYYYYMMDD.Size = new System.Drawing.Size(269, 24);
            this.checkBoxCreateFoldersYYYYMMDD.TabIndex = 11;
            this.checkBoxCreateFoldersYYYYMMDD.Text = "Create child folders YYYYMMDD";
            this.checkBoxCreateFoldersYYYYMMDD.UseVisualStyleBackColor = true;
            // 
            // RenameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 469);
            this.Controls.Add(this.checkBoxCreateFoldersYYYYMMDD);
            this.Controls.Add(this.checkBoxMoveToNewFolder);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtOutPutFolder);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnRename);
            this.Controls.Add(this.btnBrowser);
            this.Controls.Add(this.txtFolder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "RenameForm";
            this.Text = "RenameForm";
            this.Load += new System.EventHandler(this.RenameForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Button btnBrowser;
        private System.Windows.Forms.Button btnRename;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtOutPutFolder;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxMoveToNewFolder;
        private System.Windows.Forms.CheckBox checkBoxCreateFoldersYYYYMMDD;
    }
}