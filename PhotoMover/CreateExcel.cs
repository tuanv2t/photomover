﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using IOHelper;

namespace PhotoMover
{
    public partial class CreateExcel : Form
    {
        public CreateExcel()
        {
            InitializeComponent();
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            string[] subdirectoryEntries = Directory.GetDirectories(txtFolder.Text);
            //var excelFilePath = $"{Directory.GetCurrentDirectory()}\\{ Path.GetFileName(txtFolder.Text)}_{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.xlsx";
            var excelFilePath = $"{txtFolder.Text}\\{ Path.GetFileName(txtFolder.Text)}_{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.xlsx";


            using (FileStream stream = new FileStream(excelFilePath, FileMode.Create, FileAccess.Write))
            {
                IWorkbook wb = new XSSFWorkbook();
                ISheet sheet = wb.CreateSheet(Path.GetFileName(txtFolder.Text));
                //Create header 
                IRow rowHeader = sheet.CreateRow(0);
                CreateHeader(rowHeader);

                for (int i = 0; i < subdirectoryEntries.Length; i++)
                {
                    IRow row = sheet.CreateRow(i+1);
                   

                    ICell cell2 = row.CreateCell(0);//folder name
                    cell2.SetCellValue(Path.GetFileName(subdirectoryEntries[i]));

                    ICell cell = row.CreateCell(1);//YYYYMMDD
                    cell.SetCellValue(Path.GetFileName(subdirectoryEntries[i]).Substring(0, 8));

                    ICell cellYYYY = row.CreateCell(2);//YYYYMMDD
                    cellYYYY.SetCellValue(Path.GetFileName(subdirectoryEntries[i]).Substring(0, 4));

                    ICell cellMM = row.CreateCell(3);//MM
                    cellMM.SetCellValue(Path.GetFileName(subdirectoryEntries[i]).Substring(4, 2));

                    ICell cellDD = row.CreateCell(4);//DD
                    cellDD.SetCellValue(Path.GetFileName(subdirectoryEntries[i]).Substring(6, 2));

                    var size = FileHelper.GetFileSizeSumFromDirectory(subdirectoryEntries[i]);
                    ICell cellSize = row.CreateCell(5);
                    cellSize.SetCellValue(size.ToString());

                    ICell cellFB = row.CreateCell(6);
                    cellFB.SetCellValue("");

                    ICell cellFullHD = row.CreateCell(7);
                    cellFullHD.SetCellValue("");


                    ICell cellCloud1 = row.CreateCell(8);
                    cellCloud1.SetCellValue("");

                    ICell cellCloud2 = row.CreateCell(9);
                    cellCloud2.SetCellValue("");

                    ICell cellFullHDYoutube = row.CreateCell(10);
                    cellFullHDYoutube.SetCellValue("");



                    ICell cellPlace = row.CreateCell(11);
                    cellPlace.SetCellValue("");

                    ICell cellTag = row.CreateCell(12);
                    cellTag.SetCellValue("");

                    ICell cellDesc = row.CreateCell(13);
                    cellDesc.SetCellValue("");

                }
                wb.Write(stream);
            }
            MessageBox.Show("Done");
        }

        private void CreateHeader(IRow rowHeader)
        {
            ICell cellName = rowHeader.CreateCell(0);
            cellName.SetCellValue("Name");

            ICell cellYYYYMMDD = rowHeader.CreateCell(1);
            cellYYYYMMDD.SetCellValue("YYYYMMDD");

            ICell cellYYYY = rowHeader.CreateCell(2);
            cellYYYY.SetCellValue("YYYY");

            ICell cellMM = rowHeader.CreateCell(3);
            cellMM.SetCellValue("MM");

            ICell cellDD = rowHeader.CreateCell(4);
            cellDD.SetCellValue("DD");

            ICell cellSize = rowHeader.CreateCell(5);
            cellSize.SetCellValue("Size");

            ICell cellFB = rowHeader.CreateCell(6);
            cellFB.SetCellValue("Facebook");

            ICell cellFullHD = rowHeader.CreateCell(7);
            cellFullHD.SetCellValue("FullHDFacebook");


            ICell cellCloud1 = rowHeader.CreateCell(8);
            cellCloud1.SetCellValue("Cloud1");

            ICell cellCloud2 = rowHeader.CreateCell(9);
            cellCloud2.SetCellValue("Cloud2");

            ICell cellFullHDYoutube = rowHeader.CreateCell(10);
            cellFullHDYoutube.SetCellValue("FullHDYoutube");

        

            ICell cellPlace = rowHeader.CreateCell(11);
            cellPlace.SetCellValue("Place");

            ICell cellTag = rowHeader.CreateCell(12);
            cellTag.SetCellValue("Tag");

            ICell cellDesc = rowHeader.CreateCell(13);
            cellDesc.SetCellValue("Description");

        }
    }
}
