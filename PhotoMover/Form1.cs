﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoMover
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private RenameForm renameForm = null;
        private void btnRename_Click(object sender, EventArgs e)
        {
            if (renameForm == null)
            {
                renameForm = new RenameForm();
            }
            renameForm.ShowDialog();
        }
        private RenameViber renameViberForm = null;
        private void button1_Click(object sender, EventArgs e)
        {
            if (renameViberForm == null)
            {
                renameViberForm = new RenameViber();
            }
            renameViberForm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var f = new CreateFolders();
            f.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var f = new FolderSize();
            f.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var f = new RecoverLittleFolders();
            f.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var f = new MoveFiles();
            f.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var f = new CreateExcel();
            f.ShowDialog();
        }
    }
}
